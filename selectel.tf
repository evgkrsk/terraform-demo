resource "selectel_vpc_project_v2" "demo" {
  name = "demo"
  quotas {
    resource_name = "compute_cores"
    resource_quotas {
      region = var.region
      zone   = var.az_zone
      value  = var.quota_cores
    }
  }
  quotas {
    resource_name = "compute_ram"
    resource_quotas {
      region = var.region
      zone   = var.az_zone
      value  = var.quota_ram
    }
  }
  quotas {
    resource_name = var.quota_disk_name
    resource_quotas {
      region = var.region
      zone   = var.az_zone
      value  = var.quota_disk
    }
  }
}

resource "selectel_vpc_user_v2" "user1" {
  name     = "user1"
  password = var.user1_pass
  enabled  = true
}

resource "selectel_vpc_role_v2" "role_user1_acc_demo" {
  project_id = selectel_vpc_project_v2.demo.id
  user_id    = selectel_vpc_user_v2.user1.id
}

resource "selectel_vpc_keypair_v2" "user1_keypair1" {
  name       = "user1_keypair1"
  public_key = var.user1_public_key
  user_id    = selectel_vpc_user_v2.user1.id
}

resource "selectel_vpc_floatingip_v2" "floatingip_1" {
  project_id = selectel_vpc_project_v2.demo.id
  region     = var.region
}
