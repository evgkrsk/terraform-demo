terraform {
  required_version = ">= 1.0.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.49.0"
    }
    selectel = {
      source  = "selectel/selectel"
      version = "~> 3.9.0"
    }
    random = {
      source  = "random"
      version = "~> 3.4.3"
    }
  }
}

provider "openstack" {
  auth_url    = var.openstack_auth_url
  domain_name = var.openstack_domain_name
  tenant_id   = selectel_vpc_project_v2.demo.id
  user_name   = selectel_vpc_user_v2.user1.name
  password    = selectel_vpc_user_v2.user1.password
  region      = var.region
}
