variable "openstack_auth_url" {
  default     = "https://api.selvpc.ru/identity/v3"
  type        = string
  description = "URL подключения к API Openstack-а"
}

variable "openstack_domain_name" {
  default     = "12345" # Подставьте свой номер договора
  type        = string
  description = "Номер договора с Selectel для авторизации в Openstack"
}

variable "region" {
  default     = "ru-3"
  type        = string
  description = "Пул Облачной платформы"
}

variable "user1_public_key" {
  default     = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC54GT6dp/19C82CNEcIk74q78KRC7XnVU4Thew0x7MhRg5SEbKVgybbv2gVF/7q7b7FePXig8NfdsTP5YdAWA1HXHl0NsXh0YJUDjTVNm3bcsFmwAhTcNzLZ6+qAWP4CzsLyZhyKYxHEjNc62HZM+gtSZxC1+NXMfV2RjgVVXQWJUZol6tzozcQ0tKedY/ygvSFS2J1eitPckRUMHl5Rk3Eag/S+nuo48C8P9nJy5P/60rLtq5jn4keTotSl7wuZF+ZhZn2eVxqD0rh1QlUiSam3TzJ302JyZOYe3quTQrS2Lk/nGxVOonH1l0eRwvRQkGWgm8X1lqTdG4KF4CsErd5/NE7O6fRT100zBxwvuhqIMUbrn7uTmvdIcqbbC09dg7SHtaJch2O2Oz8rx0X5KQhQIaC6husMxRABiD+sDBvECjW8NiwNKvbMfi2ugXcO2MZ3H8VmF0PmPBuHP/BCj+/FMKnSaPek5d65JQSmB+3MLr/4Eoqt4VQNbJKTSlEZE= evg@iMac-Evgenij.local"
  type        = string
  description = "Значение SSH-ключа для доступа к облачному серверу"
}

variable "az_zone" {
  default     = "ru-3b"
  type        = string
  description = "Сегмент пула"
}

variable "volume_type" {
  default     = "fast.ru-3b"
  type        = string
  description = "Тип сетевого диска, из которого создается сервер"
}

variable "subnet_cidr" {
  default     = "10.10.0.0/24"
  type        = string
  description = "CIDR подсети"
}

variable "user1_pass" {
  default     = "qoPqi5-fagwic-ryccud-vamlas-kiscif-zajvek" # change this in TF_VAR_user1_pass
  type        = string
  description = "Пароль пользователя user1 открытым текстом"
}

variable "quota_cores" {
  type        = number
  default     = 2
  description = "Квота в ядрах ЦПУ"
}

variable "quota_ram" {
  type        = number
  default     = 2048
  description = "Квота памяти в мегабайтах"
}

variable "quota_disk" {
  type        = number
  default     = 10
  description = "Квота диска в гигабайтах"
}

variable "quota_disk_name" {
  type        = string
  default     = "volume_gigabytes_fast"
  description = "Тип диска для квоты"
}
